/*
 * Copyright (C) 2018 Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package servlets;

import beans.selectFromDataBaseBean.SelectFromDataBaseBeanRemote;
import exceptions.WrongInputException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet for displaying records from the database.
 *
 * @author Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 */
public class ViewListServlet extends HttpServlet {

    @EJB
    SelectFromDataBaseBeanRemote selectBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        boolean searchByValue = false;
        boolean searchByName = false;

        String min = null;
        String max = null;
        if (request.getParameter("searchByValue") != null) {
            searchByValue = true;
        } else if (request.getParameter("searchByName") != null) {
            searchByName = true;
        }

        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet View</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet View at " + request.getContextPath() + "</h1>");
            if (searchByValue) {
                boolean maxEmptyFlag = false;
                boolean minEmptyFlag = false;

                // Max parameter.
                max = request.getParameter("paramMax");
                if (max == null || (max.trim().isEmpty())) {
                    maxEmptyFlag = true;
                }
                // Min parameter.
                min = request.getParameter("paramMin");
                if (min == null || (min.trim().isEmpty())) {
                    minEmptyFlag = true;
                }
                // There is no form.
                if (minEmptyFlag & maxEmptyFlag) {
                    out.println("<p>Entered empty min and max values.</p>");
                    getViewListForm(out);
                } else if (minEmptyFlag) {
                    out.println("<p>Entered empty min value.</p>");
                    getViewListForm(out);
                } else if (maxEmptyFlag) {
                    out.println("<p>Entered empty max values.</p>");
                    getViewListForm(out);
                } else {
                    try {
                        Map<String, Integer> map = selectBean.getRecordsByValue(min, max);
                        getRecordsTable(out, map);
                    } catch (WrongInputException input) {
                        switch (input.getMessage()) {
                            case "nullMinAndMax":
                                out.println("<p>Entered empty min and max values.</p>");
                                getViewListForm(out);
                                break;
                            case "nullMin":
                                out.println("<p>Entered empty min value.</p>");
                                getViewListForm(out);
                                break;
                            case "nullMax":
                                out.println("<p>Entered empty max values.</p>");
                                getViewListForm(out);
                                break;
                            case "wrongMinAndMax":
                                out.println("<p>Entered wrong min and max values.</p>");
                                getViewListForm(out);
                                break;
                            case "wrongMin":
                                out.println("<p>Entered wrong min value.</p>");
                                getViewListForm(out);
                                break;
                            case "wrongMax":
                                out.println("<p>Entered wrong max value.</p>");
                                getViewListForm(out);
                                break;
                            default:
                                out.println("<p>Opps!</p>");
                                getViewListForm(out);
                                break;
                        }
                    } catch (Exception e) {
                        out.println(e.getMessage());
                    }
                }
            } else if (searchByName) {
                // Name parameter.
                max = request.getParameter("paramName");
                if (max == null || (max.trim().isEmpty())) {
                    out.println("<p>Entered empty name/mask.</p>");
                    getViewListForm(out);
                } else {
                    try {
                        Map<String, Integer> map = selectBean.getRecordsByName(max);
                        getRecordsTable(out, map);
                    } catch (WrongInputException input) {
                        switch (input.getMessage()) {
                            case "wrongName":
                                out.println("<p>Entered wrong  name/mask.</p>");
                                getViewListForm(out);
                                break;
                            case "emptyName":
                                out.println("<p>You have found an empty name mistake!</p>");
                                getViewListForm(out);
                                break;
                            default:
                                out.println("<p>Opps!</p>");
                                getViewListForm(out);
                                break;
                        }
                    } catch (Exception e) {
                        out.println(e.getMessage());
                    }
                }
            } else {
                try {
                    Map<String, Integer> map = selectBean.getRecords();
                    getRecordsTable(out, map);
                } catch (Exception e) {
                    out.println(e.getMessage());
                }
            }
            getSearchForValueForm(out);
            getSearchForNameForm(out);
            getRegistratorForm(out);
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Print the search form by value.
     *
     * @param out response PrintWriter
     */
    private void getSearchForValueForm(PrintWriter out) {
        out.println("<br/>");
        out.println("\t<form action='ViewList' method='GET'>");
        out.println("\t\tMin: <input type='text' name='paramMin' size='25'/> ");
        out.println("\t\tMax: <input type='text' name='paramMax' size='25'/> ");
        out.println("\t\t<input type='submit' name='searchByValue' value='Search By Value'/>");
        out.println("\t</form>");
    }

    /**
     * Print the search form by name.
     *
     * @param out response PrintWriter
     */
    private void getSearchForNameForm(PrintWriter out) {
        out.println("<br/>");
        out.println("\t<form action='ViewList' method='GET'>");
        out.println("\t\tName or mask: <input type='text' name='paramName' size='25'/> ");
        out.println("\t\t<input type='submit' name='searchByName' value='Search By Name'/>");
        out.println("\t</form>");
    }

    /**
     * Print records inside a table.
     *
     * @param out response PrintWriter
     * @param map records map
     */
    private void getRecordsTable(PrintWriter out, Map<String, Integer> map) {
        int id = 1;
        out.println("<br/>");
        out.println("<table width='100%' border='1'>");
        out.println("\t<tbody>");
        out.println("\t\t<tr>");
        out.println("\t\t\t<td>ID</td><td>Tytle</td><td>Value</td><td>Delele</td><td>Modify</td>");
        out.println("\t\t</tr>");
        for (String str : map.keySet()) {
            out.println("\t\t<tr>");
            out.print("\t\t\t<td>" + id + "</td>");
            out.print("<td>" + str + "</td>");
            out.print("<td>" + map.get(str) + "</td>");
            out.print("<form action='Registrator' method='GET'>");
            out.print("<input type='hidden' name='paramName' value='" + str + "' />");
            out.print("<td><input style='width: 100%' type='submit' name='del' value='Delete' /></td>");
            out.print("</form>");
            out.print("<form action='Registrator' method='GET'>");
            out.print("<input type='hidden' name='var' value='" + str + "' />");
            out.print("<td><input style='width: 100%' type='submit' name='cng' value='Modify' /></td>");
            out.print("</form>");
            out.println("\t\t</tr>");
            id++;
        }
        out.println("\t</tbody>");
        out.println("</table>");
    }

    /**
     * Print Registrator servlet form.
     *
     * @param out response PrintWriter
     */
    private void getRegistratorForm(PrintWriter out) {
        out.println("<br/>");
        out.println("<form action='Registrator' method='GET'>");
        out.println("\t<input type='submit' name='add' value='Add new element'/>");
        out.println("</form>");
    }

    /**
     * Print ViewList servlet form.
     *
     * @param out response PrintWriter
     */
    private void getViewListForm(PrintWriter out) {
        out.println("<br/>");
        out.println("\t<form action='ViewList' method='GET'>");
        out.println("\t\t<input type='submit' name='show' value='Show all elements'/>");
        out.println("\t</form>");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
