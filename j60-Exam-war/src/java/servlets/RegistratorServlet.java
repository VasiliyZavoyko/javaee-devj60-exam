/*
 * Copyright (C) 2018 Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package servlets;

import beans.updateDataBaseBean.UpdateDataBaseBeanRemote;
import exceptions.WrongInputException;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet to transfer records to the database.
 *
 * @author Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 */
public class RegistratorServlet extends HttpServlet {

    @EJB
    private UpdateDataBaseBeanRemote updateBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        // Flags, thinking good in a beginning.
        boolean addForm = false;
        boolean delForm = false;
        boolean doDelForm = false;
        boolean cngForm = false;
        boolean doCngForm = false;

        if (request.getParameter("add") != null) {
            addForm = true;
        } else if (request.getParameter("del") != null) {
            delForm = true;
        } else if (request.getParameter("doDel") != null) {
            doDelForm = true;
        } else if (request.getParameter("cng") != null) {
            cngForm = true;
        } else if (request.getParameter("doCng") != null) {
            doCngForm = true;
        }

        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Registrator</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("Servlet Registrator at " + request.getContextPath() + "\n<br>\n");
            out.println("<hr/>");
            out.println("<br/>");
            out.println("<h3>Adding new elements.</h3>");
            out.println("<br/>");
            out.println("<br/>");

            if (addForm) {
                String name = null;
                String value = null;

                boolean nameEmptyFlag = false;
                boolean valueEmptyFlag = false;

                name = request.getParameter("paramName");
                if (name == null || (name.trim().isEmpty())) {
                    nameEmptyFlag = true;
                }
                value = request.getParameter("paramValue");
                if (value == null || (value.trim().isEmpty())) {
                    valueEmptyFlag = true;
                }

                if (nameEmptyFlag & valueEmptyFlag) {
                    out.println("<p>You have entered an empty name and value!</p>");
                } else if (nameEmptyFlag) {
                    out.println("<p>You have entered an empty name!</p>");
                } else if (valueEmptyFlag) {
                    out.println("<p>You have entered an empty value!</p>");
                } else {
                    try {
                        updateBean.addRecord(name.trim(), value.trim());
                        out.println("<p>Record " + name + " added.</p>");
                    } catch (WrongInputException input) {
                        switch (input.getMessage()) {
                            case "recordExists":
                                out.println("<p>Record " + name + " exists.</p>");
                                break;
                            case "emptyNameAndValue":
                                out.println("<p>Entered empty name and empty value.</p>");
                                break;
                            case "emptyName":
                                out.println("<p>Entered empty name.</p>");
                                break;
                            case "emptyValue":
                                out.println("<p>Entered empty value.</p>");
                                break;
                            case "wrongNameAndValue":
                                out.println("<p>Entered wrong name and wrong value.</p>");
                                break;
                            case "wrongValue":
                                out.println("<p>Entered wrong value.</p>");
                                break;
                            case "wrongName":
                                out.println("<p>Entered wrong name.</p>");
                                break;
                            default:
                                out.println("<p>Opps!</p>");
                                break;
                        }
                    } catch (Exception e) {
                        out.println(e.getMessage());
                    }
                }
                out.println("Name must be shorter than 255 characters. Value must be a number.");
                getInputForm(out);
                out.println("<br/>");
                getViewListForm(out);
            } else if (delForm) {
                String name = null;

                name = request.getParameter("paramName");
                if (name == null || (name.trim().isEmpty())) {
                    out.println("<p>You have found an empty name mistake!</p>");
                    getRegistratorForm(out);
                    getViewListForm(out);
                } else {
                    getConfirmDelForm(out, name);
                }
            } else if (doDelForm) {
                String name = null;

                name = request.getParameter("var");
                if (name == null || (name.trim().isEmpty())) {
                    out.println("<p>You have found an empty name mistake!</p>");
                    getRegistratorForm(out);
                    getViewListForm(out);
                } else {
                    try {
                        updateBean.deleteRecord(name.trim());
                        out.println("<p>Record removed.</p>");
                        getRegistratorForm(out);
                        getViewListForm(out);
                    } catch (WrongInputException input) {
                        switch (input.getMessage()) {
                            case "recordNotExists":
                                out.println("<p>Record not exists.</p>");
                                getRegistratorForm(out);
                                getViewListForm(out);
                                break;
                            default:
                                out.println("<p>Opps, my bad!</p>");
                                break;
                        }
                    } catch (Exception e) {
                        out.println(e.getMessage());
                    }
                }
            } else if (cngForm) {
                String name = null;

                name = request.getParameter("var");
                if (name == null || (name.trim().isEmpty())) {
                    out.println("<p>You have found an empty name mistake!</p>");
                    getRegistratorForm(out);
                    getViewListForm(out);
                } else {
                    getCngForm(out, name);
                }
            } else if (doCngForm) {
                String name = null;
                String value = null;
                boolean nameEmptyFlag = false;
                boolean valueEmptyFlag = false;

                name = request.getParameter("var");
                if (name == null || (name.trim().isEmpty())) {
                    nameEmptyFlag = true;
                }
                value = request.getParameter("paramValue");
                if (name == null || (name.trim().isEmpty())) {
                    nameEmptyFlag = true;
                }

                if (nameEmptyFlag & valueEmptyFlag) {
                    out.println("<p>You have found an empty name mistake and also added empty value!</p>");
                    getRegistratorForm(out);
                    getViewListForm(out);
                } else if (nameEmptyFlag) {
                    out.println("<p>You have found an empty name mistake!</p>");
                    getRegistratorForm(out);
                    getViewListForm(out);
                } else if (valueEmptyFlag) {
                    out.println("<p>You have entered an empty value!</p>");
                    getRegistratorForm(out);
                    getViewListForm(out);
                } else {
                    try {
                        updateBean.changeRecord(name.trim(), value.trim());
                        out.println("<p>Record changed.</p>");
                    } catch (WrongInputException input) {
                        switch (input.getMessage()) {
                            case "recordNotExists":
                                out.println("<p>Record not exists.</p>");
                                break;
                            case "wrongValue":
                                out.println("<p>Entered wrong value.</p>");
                                break;
                            default:
                                out.println("<p>Opps, my bad!</p>");
                                break;
                        }
                    } catch (Exception e) {
                        out.println(e.getMessage());
                    }
                }

            } else {
                getRegistratorForm(out);
                out.println("<br/>");
                getViewListForm(out);
            }

            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Print the form to confirm deletion.
     *
     * @param out response PrintWriter
     * @param name String name of record for deleting
     */
    private void getConfirmDelForm(PrintWriter out, String name) {
        out.println("\t<form action='Registrator' method='GET'>");
        out.println("\t\t<input type='hidden' name='var' value='" + name + "' />");
        out.println("\t\t<input type='submit' name='doDel' value='Delete'/>");
        out.println("\t</form>");
        out.println("\t<form action='ViewList' method='GET'>");
        out.println("\t\t<input type='submit' name='show' value='Cancel'/>");
        out.println("\t</form>");
    }

    /**
     * Print Registrator servlet form.
     *
     * @param out response PrintWriter
     */
    private void getRegistratorForm(PrintWriter out) {
        out.println("<br/>");
        out.println("\t<form action='Registrator' method='GET'>");
        out.println("\t\t<input type='submit' name='add' value='Add new element'/>");
        out.println("\t</form>");
    }

    /**
     * Print the form for entering the name and value of the record to be
     * deleted.
     *
     * @param out response PrintWriter
     */
    private void getInputForm(PrintWriter out) {
        out.println("\t<form action='Registrator' method='GET'>");
        out.println("\t\tName: <input type='text' name='paramName' size='25'/> ");
        out.println("\t\tValue: <input type='text' name='paramValue' size='25'/> ");
        out.println("\t\t<input type='submit' name='add' value='Add'/>");
        out.println("\t</form>");
    }

    /**
     * Print the form for entering the value of the record to be changed.
     *
     * @param out response PrintWriter
     * @param name
     */
    private void getCngForm(PrintWriter out, String name) {
        out.println("\t<form action='Registrator' method='GET'>");
        out.println("\t\t<input type='hidden' name='var' value='" + name + "' />");
        out.println("\t\tValue: <input type='text' name='paramValue' size='25'/> ");
        out.println("\t\t<input type='submit' name='doCng' value='Change'/>");
        out.println("\t</form>");
    }

    /**
     * Print ViewList servlet form.
     *
     * @param out response PrintWriter
     */
    private void getViewListForm(PrintWriter out) {
        out.println("<br/>");
        out.println("\t<form action='ViewList' method='GET'>");
        out.println("\t\t<input type='submit' name='show' value='Show all elements'/>");
        out.println("\t</form>");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
