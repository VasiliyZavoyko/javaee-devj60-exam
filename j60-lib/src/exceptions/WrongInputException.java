/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 * Exception for incorrect input.
 *
 * @author Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 */
public class WrongInputException extends Exception {

    private static final long serialVersionUID = 1L;

    public WrongInputException(String message) {
        super(message);
    }

}
