/*
 * Copyright (C) 2018 Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package beans.selectFromDataBaseBean;

import exceptions.WrongInputException;
import java.util.Map;
import javax.ejb.Remote;

/**
 * Remote interface for getting records from database.
 *
 * @author Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 */
@Remote
public interface SelectFromDataBaseBeanRemote {

    /**
     * Retrieve all records from the database manager.
     *
     * @return Map<String, Integer> of records from database
     * @throws Exception if something wrong with database
     */
    public Map<String, Integer> getRecords() throws Exception;

    /**
     * Search for records by name from the database manager.
     *
     * @param name String name or mask
     * @return Map<String, Integer> of found records from the database
     * @throws Exception if something wrong with database
     */
    public Map<String, Integer> getRecordsByName(String name) throws Exception, WrongInputException;

    /**
     * Search for records by values from the database manager.
     *
     * @param min String minimum value
     * @param max String maximum value
     * @return Map<String, Integer> of found records from the database
     * @throws Exception if something wrong with database
     * @throws WrongInputException if something wrong with inputed values
     */
    public Map<String, Integer> getRecordsByValue(String min, String max) throws Exception, WrongInputException;

}
