/*
 * Copyright (C) 2018 Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package beans.updateDataBaseBean;

import exceptions.WrongInputException;
import javax.ejb.Remote;

/**
 * Remote interface for adding/removing/changing records into database.
 *
 * @author Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 */
@Remote
public interface UpdateDataBaseBeanRemote {

    /**
     * Adding new record into database.
     *
     * @param name String name of record
     * @param value int value of record
     * @throws Exception if something wrong with database
     * @throws WrongInputException if something wrong with inputed values
     */
    public void addRecord(String name, String value) throws Exception, WrongInputException;

    /**
     * Remove existing record from database.
     *
     * @param name String name of existing record
     * @throws Exception if something wrong with database
     * @throws WrongInputException if something wrong with inputed values
     */
    public void deleteRecord(String name) throws Exception, WrongInputException;

    /**
     * Change the value of an existing record.
     *
     * @param name String name of existing record
     * @param value new int value of record
     * @throws Exception if something wrong with database
     * @throws WrongInputException if something wrong with inputed values
     */
    public void changeRecord(String name, String value) throws Exception, WrongInputException;

}
