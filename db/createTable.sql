CREATE TABLE records (
id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),
rec_name VARCHAR(255) NOT NULL,
rec_value INTEGER NOT NULL,
CONSTRAINT pk_name PRIMARY KEY(rec_name)
);

INSERT INTO ROOT.records (rec_name, rec_value) VALUES ('testName1', 1);
INSERT INTO ROOT.records (rec_name, rec_value) VALUES ('testName2', 2);
INSERT INTO ROOT.records (rec_name, rec_value) VALUES ('testName3', 3);
