package record;

/*
 * Copyright (C) 2018 Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Database record entity.
 *
 * @author Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 */
@Entity
@Table(name = "RECORDS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Records.findAll", query = "SELECT r FROM Records r")
    , @NamedQuery(name = "Records.findById", query = "SELECT r FROM Records r WHERE r.id = :id")
    , @NamedQuery(name = "Records.findByRecName", query = "SELECT r FROM Records r WHERE r.recName = :recName")
    , @NamedQuery(name = "Records.findByRecValue", query = "SELECT r FROM Records r WHERE r.recValue = :recValue")
    , @NamedQuery(name = "Records.findBetweenTwoRecValues", query = "SELECT r FROM Records r WHERE r.recValue BETWEEN :minValue AND :maxValue")})
public class Records implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "REC_NAME")
    private String recName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REC_VALUE")
    private int recValue;

    public Records() {
    }

    public Records(String recName) {
        this.recName = recName;
    }

    public Records(String recName, int recValue) {
        this.recName = recName;
        this.recValue = recValue;
    }

    public Records(String recName, int id, int recValue) {
        this.recName = recName;
        this.id = id;
        this.recValue = recValue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRecName() {
        return recName;
    }

    public void setRecName(String recName) {
        this.recName = recName;
    }

    public int getRecValue() {
        return recValue;
    }

    public void setRecValue(int recValue) {
        this.recValue = recValue;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (recName != null ? recName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Records)) {
            return false;
        }
        Records other = (Records) object;
        if ((this.recName == null && other.recName != null) || (this.recName != null && !this.recName.equals(other.recName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.testesBeans.Records[ recName=" + recName + " ]";
    }

}
