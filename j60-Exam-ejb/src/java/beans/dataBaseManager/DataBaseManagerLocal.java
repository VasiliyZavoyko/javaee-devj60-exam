package beans.dataBaseManager;

/*
 * Copyright (C) 2018 Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import exceptions.WrongInputException;
import java.util.Collection;
import javax.ejb.Local;
import record.Records;

/**
 * Database manager local interface.
 *
 * @author Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 */
@Local
public interface DataBaseManagerLocal {

    /**
     * Get all records from the database.
     *
     * @return Collection<Records> of records from database
     * @throws Exception if something wrong with database
     */
    public Collection<Records> getAll() throws Exception;

    /**
     * Get all records, with values between minimum and maximum values, from the
     * database.
     *
     * @param min int minimul value
     * @param max int maximum value
     * @return Collection<Records> of found records from the database
     * @throws Exception if something wrong with database
     */
    public Collection<Records> getByValue(int min, int max) throws Exception;

    /**
     * Get an entry with a particular name from the database.
     *
     * @param name String name of record
     * @return record with the name
     * @throws Exception if something wrong with database
     * @throws WrongInputException there is no such name in the database
     */
    public Records getByName(String name) throws Exception;

    /**
     * Add record into database.
     *
     * @param name String record name
     * @param value int record value
     * @throws Exception if something wrong with database
     * @throws WrongInputException if record exists in database
     */
    public void add(String name, int value) throws Exception, WrongInputException;

    /**
     * Update record value into database.
     *
     * @param name String record name
     * @param value int new record value
     * @throws Exception if something wrong with database
     * @throws WrongInputException if record does not exists in database
     */
    public void update(String name, int value) throws Exception, WrongInputException;

    /**
     * Remove record from database.
     *
     * @param name String name of record to remove
     * @throws Exception if something wrong with database
     * @throws WrongInputException if record does not exists in database
     */
    public void remove(String name) throws Exception, WrongInputException;

}
