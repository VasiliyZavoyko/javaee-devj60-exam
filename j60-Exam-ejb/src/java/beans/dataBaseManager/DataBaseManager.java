package beans.dataBaseManager;

/*
 * Copyright (C) 2018 Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import exceptions.WrongInputException;
import java.util.Collection;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import record.Records;

/**
 * Database manager bean.
 *
 * @author Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 */
@Singleton
public class DataBaseManager implements DataBaseManagerLocal {

    private EntityManagerFactory factory;
    private EntityManager manager;

    /**
     * Initialize database resource.
     */
    @PostConstruct
    public void init() {
        factory = Persistence.createEntityManagerFactory("j60-Exam-ejbPU");
        manager = factory.createEntityManager();
    }

    /**
     * Close database resource.
     */
    @PreDestroy
    public void exit() {
        manager.close();
        factory.close();
    }

    /**
     * Get all records from the database.
     *
     * @return Collection<Records> of records from database
     * @throws Exception if something wrong with database
     */
    @Override
    @SuppressWarnings("unchecked")
    public Collection<Records> getAll() throws Exception {
        List<Records> list = null;
        try {
            Query query = manager.createNamedQuery("Records.findAll");
            list = query.getResultList();
        } catch (Exception e) {
            throw new Exception("Database manager error while getting records!", e);
        }
        return list;
    }

    /**
     * Add record into database.
     *
     * @param name String record name
     * @param value int record value
     * @throws Exception if something wrong with database
     * @throws WrongInputException if record exists in database
     */
    @Override
    public void add(String name, int value) throws Exception, WrongInputException {
        EntityTransaction transaction = null;
        try {
            Records record = manager.find(Records.class, name);
            if (record == null) {
                transaction = manager.getTransaction();
                transaction.begin();
                manager.persist(new Records(name, value));
                transaction.commit();
            } else {
                throw new WrongInputException("recordExists");
            }
        } catch (WrongInputException input) {
            throw input;
        } catch (Exception e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            throw new Exception("Database manager error while committing new record!", e);
        }
    }

    /**
     * Update record value into database.
     *
     * @param name String record name
     * @param value int new record value
     * @throws Exception if something wrong with database
     * @throws WrongInputException if record does not exists in database
     */
    @Override
    public void update(String name, int value) throws Exception, WrongInputException {
        EntityTransaction transaction = null;
        try {
            Records record = manager.find(Records.class, name);
            if (record != null) {
                transaction = manager.getTransaction();
                transaction.begin();
                record.setRecValue(value);
                transaction.commit();
            } else {
                throw new WrongInputException("recordNotExists");
            }
        } catch (WrongInputException input) {
            throw input;
        } catch (Exception e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            throw new Exception("Database manager error while updating record!", e);
        }
    }

    /**
     * Remove record from database.
     *
     * @param name String name of record to remove
     * @throws Exception if something wrong with database
     * @throws WrongInputException if record does not exists in database
     */
    @Override
    public void remove(String name) throws Exception, WrongInputException {
        EntityTransaction transaction = null;
        try {
            Records record = manager.find(Records.class, name);
            if (record != null) {
                transaction = manager.getTransaction();
                transaction.begin();
                manager.remove(record);
                transaction.commit();
            } else {
                throw new WrongInputException("recordNotExists");
            }
        } catch (WrongInputException input) {
            throw input;
        } catch (Exception e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            throw new Exception("Database manager error while deleting record!", e);
        }
    }

    /**
     * Get all records, with values between minimum and maximum values, from the
     * database.
     *
     * @param min int minimul value
     * @param max int maximum value
     * @return Collection<Records> of found records from the database
     * @throws Exception if something wrong with database
     */
    @Override
    @SuppressWarnings("unchecked")
    public Collection<Records> getByValue(int min, int max) throws Exception {
        List<Records> list = null;
        try {
            Query query = manager.createNamedQuery("Records.findBetweenTwoRecValues").setParameter("minValue", min).setParameter("maxValue", max);
            list = query.getResultList();
        } catch (Exception e) {
            throw new Exception("Database manager error while getting records!", e);
        }
        return list;
    }

    /**
     * Get an entry with a particular name from the database.
     *
     * @param name String name of record
     * @return record with the name
     * @throws Exception if something wrong with database
     * @throws WrongInputException there is no such name in the database
     */
    @Override
    @SuppressWarnings("unchecked")
    public Records getByName(String name) throws Exception, WrongInputException {
        Records record = null;
        try {
            record = manager.find(Records.class, name);
            if (record == null) {
                throw new WrongInputException("recordNotExists");
            }
        } catch (WrongInputException input) {
            throw input;
        } catch (Exception e) {
            throw new Exception("Database manager error while getting record by name!", e);
        }
        return record;
    }

}
