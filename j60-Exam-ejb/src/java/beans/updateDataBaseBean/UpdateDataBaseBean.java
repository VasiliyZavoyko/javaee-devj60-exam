/*
 * Copyright (C) 2018 Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package beans.updateDataBaseBean;

import beans.dataBaseManager.DataBaseManagerLocal;
import exceptions.WrongInputException;
import javax.ejb.EJB;
import javax.ejb.Stateful;

/**
 * Bean for adding/removing/changing records into database.
 *
 * @author Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 */
@Stateful
public class UpdateDataBaseBean implements UpdateDataBaseBeanRemote {

    @EJB
    DataBaseManagerLocal dataBaseManager;

    /**
     * Adding new record into database.
     *
     * @param name String name of record
     * @param value int value of record
     * @throws Exception if something wrong with database
     * @throws WrongInputException if something wrong with inputed values
     */
    @Override
    public void addRecord(String name, String value) throws Exception, WrongInputException {
        boolean errorName = false;
        boolean errorValue = false;

        if (name.length() > 255) {
            errorName = true;
        }

        int intValue = 0;
        try {
            intValue = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            errorValue = true;
        }

        if (errorName && errorValue) {
            throw new WrongInputException("wrongNameAndValue");
        } else if (errorValue) {
            throw new WrongInputException("wrongValue");
        } else if (errorName) {
            throw new WrongInputException("wrongName");
        }

        try {
            dataBaseManager.add(name, intValue);
        } catch (WrongInputException input) {
            switch (input.getMessage()) {
                case "recordExists":
                    throw input;
                default:
                    throw new Exception("Unusual error while adding new record!");
            }
        } catch (Exception e) {
            throw new Exception("Database error!", e);
        }
    }

    /**
     * Remove existing record from database.
     *
     * @param name String name of existing record
     * @throws Exception if something wrong with database
     * @throws WrongInputException if something wrong with inputed values
     */
    @Override
    public void deleteRecord(String name) throws Exception, WrongInputException {
        try {
            dataBaseManager.remove(name);
        } catch (WrongInputException input) {
            switch (input.getMessage()) {
                case "recordNotExists":
                    throw input;
                default:
                    throw new Exception("Unusual error while deleting record!");
            }
        } catch (Exception e) {
            throw new Exception("Database error!", e);
        }
    }

    /**
     * Change the value of an existing record.
     *
     * @param name String name of existing record
     * @param value new int value of record
     * @throws Exception if something wrong with database
     * @throws WrongInputException if something wrong with inputed values
     */
    @Override
    public void changeRecord(String name, String value) throws Exception, WrongInputException {
        int intValue = 0;
        try {
            intValue = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new WrongInputException("wrongValue");
        }

        try {
            dataBaseManager.update(name, intValue);
        } catch (WrongInputException input) {
            switch (input.getMessage()) {
                case "recordNotExists":
                    throw input;
                default:
                    throw new Exception("Unusual error while changing record!");
            }
        } catch (Exception e) {
            throw new Exception("Database error!", e);
        }
    }

}
