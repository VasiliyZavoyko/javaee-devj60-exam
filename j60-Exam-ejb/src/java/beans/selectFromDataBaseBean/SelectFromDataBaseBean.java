package beans.selectFromDataBaseBean;

/*
 * Copyright (C) 2018 Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import beans.dataBaseManager.DataBaseManagerLocal;
import exceptions.WrongInputException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import record.Records;

/**
 * Bean for getting records from database.
 *
 * @author Vasiliy Zavoyko <vasiliy.zavoyko@gmail.com>
 */
@Stateless
public class SelectFromDataBaseBean implements SelectFromDataBaseBeanRemote {

    @EJB
    DataBaseManagerLocal dBRemote;

    /**
     * Retrieve all records from the database manager.
     *
     * @return Map<String, Integer> of records from database
     * @throws Exception if something wrong with database
     */
    @Override
    public Map<String, Integer> getRecords() throws Exception {
        Map<String, Integer> out = new HashMap<>();
        try {
            Collection<Records> records = dBRemote.getAll();
            for (Records record : records) {
                out.put(record.getRecName(), record.getRecValue());
            }
        } catch (Exception e) {
            throw new Exception("Database exception", e);
        }
        return out;
    }

    /**
     * Search for records by name from the database manager.
     *
     * @param name String name or mask
     * @return Map<String, Integer> of found records from the database
     * @throws Exception if something wrong with database
     */
    @Override
    public Map<String, Integer> getRecordsByName(String name) throws Exception {
        Map<String, Integer> out = new HashMap<>();
        if (name.isEmpty()) {
            throw new WrongInputException("emptyName");
        } else {
            try {
                Records record = dBRemote.getByName(name);
                out.put(record.getRecName(), record.getRecValue());
            } catch (WrongInputException input) {
                Collection<Records> records = dBRemote.getAll();
                if (records.isEmpty()) {
                    return out;
                }
                try {
                    Pattern pattern = Pattern.compile(name);
                    for (Records record : records) {
                        if (pattern.matcher(record.getRecName()).find()) {
                            out.put(record.getRecName(), record.getRecValue());
                        }
                    }
                } catch (PatternSyntaxException exception) {
                    throw exception;
                }
            } catch (PatternSyntaxException exception) {
                throw new WrongInputException("badMask");
            } catch (Exception e) {
                throw new Exception("Database exception", e);
            }
        }
        return out;
    }

    /**
     * Search for records by values from the database manager.
     *
     * @param min String minimum value
     * @param max String maximum value
     * @return Map<String, Integer> of found records from the database
     * @throws Exception if something wrong with database
     * @throws WrongInputException if something wrong with inputed values
     */
    @Override
    public Map<String, Integer> getRecordsByValue(String min, String max) throws Exception, WrongInputException {
        if (min.isEmpty() && max.isEmpty()) {
            throw new WrongInputException("nullMinAndMax");
        } else if (min.isEmpty()) {
            throw new WrongInputException("nullMin");
        } else if (max.isEmpty()) {
            throw new WrongInputException("nullMax");
        }

        boolean errorMin = false;
        boolean errorMax = false;

        int minValue = 0;
        try {
            minValue = Integer.parseInt(min);
        } catch (NumberFormatException e) {
            errorMin = true;
        }

        int maxValue = 0;
        try {
            maxValue = Integer.parseInt(max);
        } catch (NumberFormatException e) {
            errorMax = true;
        }

        if (errorMin && errorMax) {
            throw new WrongInputException("wrongMinAndMax");
        } else if (errorMin) {
            throw new WrongInputException("wrongMin");
        } else if (errorMax) {
            throw new WrongInputException("wrongMax");
        }

        Map<String, Integer> out = new HashMap<>();
        try {
            Collection<Records> records = dBRemote.getByValue(minValue, maxValue);
            for (Records record : records) {
                out.put(record.getRecName(), record.getRecValue());
            }
        } catch (Exception e) {
            throw new Exception("Database exception", e);
        }
        return out;
    }

}
